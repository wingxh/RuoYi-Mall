package com.cyl.h5.pojo.dto;

import lombok.Data;

@Data
public class H5LoginDTO {

  private String data;
  private String key;
  private String sessionKey;
  private String openId;
}
