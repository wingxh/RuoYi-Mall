package com.cyl.h5.pojo.response;

import lombok.Data;

/**
 * @Author: czc
 * @Description: TODO
 * @DateTime: 2023/6/16 14:54
 **/
@Data
public class H5LoginResponse {
    private String token;
}
