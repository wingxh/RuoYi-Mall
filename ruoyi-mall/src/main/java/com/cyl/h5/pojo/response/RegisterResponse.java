package com.cyl.h5.pojo.response;

import lombok.Data;

@Data
public class RegisterResponse {
    /** token */
    private String token;
}
