package com.cyl.h5.pojo.request;

import lombok.Data;

@Data
public class H5LoginRequest {
    /** 账号即手机号 */
    private String mobile;
}
