package com.cyl.h5.pojo.vo.query;

import lombok.Data;

@Data
public class OrderH5Query {
    private Integer tab;
}
